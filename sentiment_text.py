import pymongo
import requests
import json
from datetime import datetime

myclient = pymongo.MongoClient("mongodb://30.30.30.44:27017/")
mydb = myclient['sosmon_ori']
ts = mydb['text_sentiment']

def pushSentiment(id, id_content, user_post, source_post, category_post, post):
    sent_data = {}
    if not post:
        sent_data['text'] = " "
    else:
        sent_data['text'] = post
    req = requests.post('http://30.30.30.45:8000/', sent_data, headers={'Content-Type': 'application/x-www-form-urlencoded'})
    res = json.loads(req.text)
    data_sentiment = {}
    data_sentiment['content_id'] = id_content
    data_sentiment['user_post'] = user_post
    data_sentiment['source_post'] = source_post
    data_sentiment['category_post'] = category_post
    data_sentiment['post'] = post
    data_sentiment['sentiment'] = res['kelas']
    data_sentiment['tgl_proccess'] = datetime.today().strftime('%Y-%m-%d')
    ts.insert_one(data_sentiment)
    print(post, ' sentiment: ', res['kelas'])
    addTagSentiment(id)

def addTagSentiment(id):
    mydict = {}
    mydict['tag_sentiment'] = True
    mydb['ori_data'].find_one_and_update({"_id": id}, {"$set": mydict})