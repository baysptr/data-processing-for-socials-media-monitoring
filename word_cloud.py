import nltk
from nltk.corpus import stopwords
from push_data import pushWord, addTagWoc
import re
import string
import pymongo
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

myclient = pymongo.MongoClient("mongodb://30.30.30.44:27017/")
mydb = myclient['sosmon_ori']
bad_word = mydb['bad_word']
factory = StemmerFactory()
stemmer = factory.create_stemmer()
bad_stopword = list()

def pullBadWord():
    bad_stopword.clear()
    query = bad_word.find()
    st = 0
    for i in query:
        bad_stopword.insert(st, i['word'].lower())
        st += 1

def removeStopwords(tokens):
    pullBadWord()
    listStopword = set(stopwords.words('indonesian'))
    listStopword2 = set(stopwords.words('english'))
    listStopword3 = set(bad_stopword)
    listStopword.update(listStopword2)
    listStopword.update(listStopword3)
    removed = []
    for t in tokens:
        if t not in listStopword:
            removed.append(t)
    return removed

# kal = "[MOJOK.co] Manfaat jogging setiap pagi yang pertama adalah meredakan stres. Olahraga itu seperti kode bagi tubuh untuk memproduksi hormon endorfin, agen perangsang rasa bahagia."

# kal = "Jakarta - Menteri Desa, Pembangunan Daerah Tertinggal dan Transmigrasi Abdul Halim Iskandar melakukan kunjungan kerja ke Kalimantan Barat. Ia mewakili Presiden RI Joko Widodo menyerahkan sertifikat sebanyak 700 bidang kepada transmigran di Kawasan Perkotaan Baru (KPB) Rasau Jaya, Kabupaten Kubu Raya, Kalimantan Barat. Penyerahan sertifikat diberikan secara simbolik kepada 15 transmigran di Islamic Centre yang ada di KPB Rasau Jaya. Adapun sertifikat yang diberikan kepada transmigran berupa sertifikat untuk lahan pekarangan atau tempat tinggal dan lahan usaha atau lahan pertanian. Abdul berpesan kepada para transmigran di Kubu Raya untuk tidak menjual sertifikat yang sudah diberikan tersebut. Dirinya berharap, sertifikat bisa disimpan dengan baik dan dimanfaatkan untuk hasil yang lebih produktif. Jangan dijual. Jangan disekolahkan, nanti akan banyak dana-dana bergulir di desa. Jangan kuatir, kita akan terus berupaya menyiapkan dana untuk bergulir di desa. Karena itu, kalau tidak amat sangat terpaksa untuk kepentingan yang produktif, tolong nggak usah disekolahkan ujarnya dalam keterangannya, Jumat (10/1/2020). Selain menyerahkan menyerahkan sertifikat tanah hak milik kepada warga, ia juga memberikan bantuan antara lain Perlengkapan dan Peralatan KBM SMK/SMA, Bantuan Perlengkapan Rumah Pintar (Komputer), DAK Afirmasi tahun 2019 (bantuan moda transportasi darat dan transportasi air), bantuan Perbaikan Kantor Pengelola KTM, Pembangunan Gudang, serta 2 paket Perlengkapan Rumah Ibadah.'Kita sudah serahkan sertifikat tanah, kita minta agar disimpan yang baik. Untuk penerima bantuan yang sudah kita serahkan, mari kita manfaatkan sebaik-baiknya untuk kesejahteraan masyarakat,' katanya."

def wordCloud(yourText, id, category, userId):
    case_folding = yourText.lower()
    # print(case_folding)

    # jadikan kata dasar dan menghilangkan nomor - nomor
    case_folding = stemmer.stem(re.sub(r'\d+', '', case_folding))
    # print(case_folding)

    # hilangkan tanda baca
    case_folding = case_folding.translate(str.maketrans('', '', string.punctuation))

    # tokenisasi / pisah kata
    tokens = nltk.tokenize.word_tokenize(case_folding)

    # print(removeStopwords(tokens))

    # hitung text
    kemunculan = nltk.FreqDist(removeStopwords(tokens))
    words = kemunculan.most_common()
    for i in words:
        pushWord(i[0], i[1], category, userId)
        addTagWoc(id)
        print(i[0], ' count: ', i[1])
    # print(kemunculan.most_common()[0][0])

# wordCloud(kal)