# Data Proccessing

Project ini adalah sebagai pendukung project platform sosial media monitoring dengan objek yang dimonitor adalah

  - Facebook
  - Twitter
  - Instagram
  - Telegram

## Fiture

  - Case Folding
  - Remove number and change to basic word
  - Remove punctuation
  - tokenizer
  - Count Word and Most Common
  - Sentiment Analysis (Netural, Negative, Positive)

## How to Install (Using Python3.7)
```sh
$ pip3 or pip install virtualenv
$ virtualenv venv
$ source venv/bin/active
$ pip install requirement.txt -r
$ python pure_data.py
```
