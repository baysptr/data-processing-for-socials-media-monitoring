import pymongo
import datetime

myclient = pymongo.MongoClient("mongodb://30.30.30.44:27017/")
mydb = myclient['sosmon_ori']
ig = mydb['word_cloud']
text_sentiment = mydb['text_sentiment']

def addTagWoc(id):
    mydict = {}
    mydict['tag_woc'] = True
    mydb['ori_data'].find_one_and_update({"_id": id}, {"$set": mydict})
    # print(mydict)

def pushWord(word, count, category, userId):
    tgl = datetime.datetime.today().strftime('%Y-%m-%d')
    # check = ig.find_one({"word": word, '$currentDate': {"tgl_proccess": {"$type": 'date'}}})
    check = ig.find_one({"word": word.upper(), "tgl_proccess": tgl})

    if check is None:
        mydict = {}
        if category == "INSTAGRAM":
            mydict['word'] = word.upper()
            mydict['instagram'] = [{"count": count, "user_id": userId}]
            mydict['twitter'] = [{"count": 0, "user_id": "0"}]
            mydict['facebook'] = [{"count": 0, "user_id": "0"}]
            mydict['telegram'] = [{"count": 0, "user_id": "0"}]
            mydict['tgl_proccess'] = tgl
        elif category == "FACEBOOK":
            mydict['word'] = word.upper()
            mydict['instagram'] = [{"count": 0, "user_id": "0"}]
            mydict['twitter'] = [{"count": 0, "user_id": "0"}]
            mydict['facebook'] = [{"count": count, "user_id": userId}]
            mydict['telegram'] = [{"count": 0, "user_id": "0"}]
            mydict['tgl_proccess'] = tgl
        elif category == "TWITTER":
            mydict['word'] = word.upper()
            mydict['instagram'] = [{"count": 0, "user_id": "0"}]
            mydict['twitter'] = [{"count": count, "user_id": userId}]
            mydict['facebook'] = [{"count": 0, "user_id": "0"}]
            mydict['telegram'] = [{"count": 0, "user_id": "0"}]
            mydict['tgl_proccess'] = tgl
        elif category == "TELEGRAM":
            mydict['word'] = word.upper()
            mydict['instagram'] = [{"count": 0, "user_id": "0"}]
            mydict['twitter'] = [{"count": 0, "user_id": "0"}]
            mydict['facebook'] = [{"count": 0, "user_id": "0"}]
            mydict['telegram'] = [{"count": count, "user_id": userId}]
            mydict['tgl_proccess'] = tgl

        ig.insert_one(mydict)
        # print(mydict)
    else:
        mydict = {}
        pushUserId = {}
        if category == "INSTAGRAM":
            mydict['count'] = count
            mydict['user_id'] = userId
            pushUserId["instagram"] = mydict
        elif category == "FACEBOOK":
            mydict['count'] = count
            mydict['user_id'] = userId
            pushUserId["facebook"] = mydict
        elif category == "TWITTER":
            mydict['count'] = count
            mydict['user_id'] = userId
            pushUserId["twitter"] = mydict
        elif category == "TELEGRAM":
            mydict['count'] = count
            mydict['user_id'] = userId
            pushUserId["telegram"] = mydict

        # print(mydict)
        ig.update_one({"_id":check['_id']}, {"$push":pushUserId})
        # print(mydict)

# pushWord("SUMATRA", 30, "FACEBOOK", '12345678')
# addTagWoc("1000745405722156251")