import pymongo
from word_cloud import wordCloud
from sentiment_text import pushSentiment

myclient = pymongo.MongoClient("mongodb://30.30.30.44:27017/")
mydb = myclient['sosmon_ori']
ig = mydb['ori_data']

# print(ig.find_one({"tag_woc": {"$exists": True}})) tag_WOC ada
# print(ig.find_one({"tag_woc": {"$exists": False}})) tag_WOC tidak ada

# check = ig.find_one({"$or": [{"tag_woc": {"$exists": False}}, {"tag_woc": False}]})
# print(check)

while True:
    # check = ig.find_one({"$or": [{"tag_woc": {"$exists": False}}, {"tag_woc": False}]})
    check = ig.find({"tag_woc": {"$exists": False}, "tag_sentiment": {"$exists": False}})
    # check = ig.find_one({"tag_woc": False})
    try:
        for i in check:
            # print(i)
            try:
                wordCloud(i['text'], i['_id'], i['source'].upper(), i['user_id'])
                pushSentiment(i['_id'], i['content_id'], i['user_id'], i['source'].upper(), i['category'].upper(), i['text'])
            except:
                print("Something wrong in structured data or value data")
    except:
        print("Error")
